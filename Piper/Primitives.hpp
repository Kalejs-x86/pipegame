#pragma once
#include <cmath>

// valid X from 0 to max value (left to right)
// valid Y from 0 to max value (top to bottom)
struct Position
{
public:
    Position() = default;
    Position(int x, int y) : x(x), y(y) {};

    bool operator==(const Position& other) const noexcept { return x == other.x && y == other.y; }
    bool operator!=(const Position& other) const noexcept { return x != other.x || y != other.y; }
    Position operator+(const Position& other) const noexcept { return Position(x + other.x, y + other.y); }

    inline int getX() const noexcept { return x; }
    inline int getY() const noexcept { return y; }
    inline void setX(int newX) noexcept { x = newX; }
    inline void setY(int newY) noexcept { y = newY; }
    inline void invalidate() noexcept { x = y = -1; }

    inline bool isValid() const noexcept { return x >= 0 && y >= 0; }
    bool operator<(const Position& other) const { return hash() < other.hash(); }
private:
    int x{ -1 };
    int y{ -1 };

    int hash() const { int Temp = (y + ((x + 1) / 2)); return x + int(std::pow(Temp, 2)); }
};

struct Size
{
public:
    Size() = default;
    Size(int width, int height) : width(width), height(height) {};

    inline int getWidth() const noexcept { return width; }
    inline int getHeight() const noexcept { return height; }
    inline void setWidth(int newWidth) noexcept { width = newWidth; }
    inline void setHeight(int newHeight) noexcept { height = newHeight; }

    inline bool isValid() const noexcept { return width >= 0 && height >= 0; }
private:
    int width{ -1 };
    int height{ -1 };
};