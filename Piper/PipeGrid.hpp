#pragma once
#include "PipePiece.hpp"
#include <set>
#include <map>
#include <vector>
#include <functional>

class PipeGrid
{
public:
    using PipeRotatedFn = std::function<void(const PipePiece&, bool)>;
    PipeRotatedFn onPipeRotated;

    PipeGrid(Size size);

    // Generates objects inside grid with a valid path
    void generate();

    void rotatePipe(Position pipePos);

    inline std::map<Position, PipePiece> getPipes() const noexcept { return pipes; }
private:
    const Size gridSize;
    Position solutionStart;
    Position solutionEnd;
    std::vector<Position> solution;
    std::map<Position, PipePiece> pipes;

    // Resets data
    void clear();

    // Creates random solutionStart and solutionEnd endpoints
    void createEndpoints();

    // Blank grid, thinks of a path that can be used as the default solution
    void createSolutionPath();

    // Creates valid pipes for the solution
    void populateSolution();

    // Returns a valid pipe that can connect both ends
    PipePiece getValidPipe(Position current, Position next, Position previous) const;

    // Creates randomized pipes for everything outside of the solution
    void populateLeftover();

    bool arePipesConnected(const PipePiece& one, const PipePiece& two) const;

    PipePiece createRandomizedPipe(const Position& pos);

    bool isSolved() const;
};

class Searcher
{
public:
    using CanMoveToFn = std::function<bool(const Position&, const Position&)>;
    Searcher(CanMoveToFn CanMoveCheckFn, const Position& start, const Position& end);

    bool search(std::list<Position>& path);
private:
    Position start, finish;

    struct Node
    {
        Node(Node* parent, Position pos) : parent(parent), pos(pos) {};
        Node* parent{ nullptr };
        Position pos;

        std::set<Position> getNeighbours();
    };

    CanMoveToFn canMoveCallback;
};