#pragma once
#include "PipePiece.hpp"
#include <SFML/Graphics.hpp>
#include <functional>

class PipeGridGraphic
{
    struct PipeGraphic
    {
        sf::Vector2f localPos;
        PipePiece::Type type;
        PipePiece::Rotation rotation;
    };
public:
    using PipeClickedFn = std::function<void(const Position)>;
    PipeClickedFn onPipeClicked;

    PipeGridGraphic(sf::RenderWindow& window, sf::Vector2f position, sf::Vector2f size, Size gridSize);

    void draw();
    void updatePipes(const std::map<Position, PipePiece> pipeData);
    void handleClick(const sf::Vector2f cursorPos);
private:
    sf::RenderWindow& window;
    sf::Vector2f positionOnScreen;
    sf::Vector2f graphicSize;
    sf::Vector2f elementSize;
    Size gridSize;
    std::map<Position, PipeGraphic> pipes;

    bool isCursorInsideComponent(const sf::Vector2f cursorPos) const;
    sf::Vector2f calcOptimalElementSize() const;

    sf::Vector2f translateToReal(sf::Vector2f localPos) const;
    sf::Vector2f translateToLocal(sf::Vector2f localPos) const;

    void drawPipePiece(const PipeGraphic& pipe);
    void drawGrid();
    void drawEmptyRect(sf::Vector2f topLeft, sf::Vector2f bottomRight, sf::Color color);
};