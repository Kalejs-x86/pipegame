#include "PipeGridGraphic.hpp"
#include <cassert>
#include <set>

namespace
{
    static void drawRightAngleLine(sf::RenderWindow& window, sf::Vector2f from, sf::Vector2f to, sf::Color color = sf::Color::Green)
    {
        sf::Vertex line[] = { {from, color}, {to, color} };
        window.draw(line, 2, sf::Lines);
        return;

        static const int lineWidth = 10;

        if (to.x < from.x || to.y < from.y)
            std::swap(from, to);

        sf::RectangleShape rect;
        sf::Vector2f size;
        sf::Vector2f position;
        if (from.y == to.y)
        {
            size = sf::Vector2f(to.x - from.x, lineWidth);
            position = sf::Vector2f(from.x, from.y - lineWidth / 2);
        }
        else if (from.x == to.x)
        {
            size = sf::Vector2f(lineWidth, to.y - from.y);
            position = sf::Vector2f(from.x - lineWidth / 2, from.y);
        }
        else
            assert(false);

        rect.setPosition(position);
        rect.setSize(size);
        rect.setFillColor(color);

        window.draw(rect);
    }
}

PipeGridGraphic::PipeGridGraphic(sf::RenderWindow & window, sf::Vector2f position, sf::Vector2f size, Size gridSize)
    : window(window)
    , graphicSize(size)
    , positionOnScreen(position)
    , gridSize(gridSize)
{
    elementSize = calcOptimalElementSize();
}

void PipeGridGraphic::draw()
{
    drawGrid();

    for (const auto& pair : pipes)
    {
        const PipeGraphic& graphic = pair.second;
        drawPipePiece(graphic);
    }

    drawEmptyRect(positionOnScreen, graphicSize, sf::Color::Red);
}

void PipeGridGraphic::updatePipes(const std::map<Position, PipePiece> pipeData)
{
    pipes.clear();
    for (const auto& pair : pipeData)
    {
        auto[gridPos, pipe] = pair;

        PipeGraphic graphic;
        graphic.rotation = pipe.getRotation();
        graphic.type = pipe.getType();

        graphic.localPos.x = gridPos.getX() * elementSize.x;
        graphic.localPos.y = gridPos.getY() * elementSize.y;

        pipes[gridPos] = graphic;
    }
}

void PipeGridGraphic::handleClick(const sf::Vector2f cursorPos)
{
    if (isCursorInsideComponent(cursorPos))
    {
        sf::Vector2f localPos = translateToLocal(cursorPos);
        Position pipePos;
        pipePos.setX(localPos.x / elementSize.x);
        pipePos.setY(localPos.y / elementSize.y);
        if (onPipeClicked)
        {
            onPipeClicked(pipePos);
        }
    }
}

bool PipeGridGraphic::isCursorInsideComponent(const sf::Vector2f cursorPos) const
{
    return cursorPos.x >= positionOnScreen.x &&
        cursorPos.y >= positionOnScreen.y &&
        cursorPos.x < positionOnScreen.x + graphicSize.x &&
        cursorPos.y < positionOnScreen.y + graphicSize.y;

}

sf::Vector2f PipeGridGraphic::calcOptimalElementSize() const
{
    sf::Vector2f optimalSize;
    optimalSize.x = graphicSize.x / (gridSize.getWidth() - 1);
    optimalSize.y = graphicSize.y / (gridSize.getHeight() - 1);


    return optimalSize;
}

sf::Vector2f PipeGridGraphic::translateToReal(sf::Vector2f localPos) const
{
    sf::Vector2f realPos;
    realPos.x = positionOnScreen.x + localPos.x;
    realPos.y = positionOnScreen.y + localPos.y;
    return realPos;
}

sf::Vector2f PipeGridGraphic::translateToLocal(sf::Vector2f realPos) const
{
    sf::Vector2f localPos;
    localPos.x = realPos.x - positionOnScreen.x;
    localPos.y = realPos.y - positionOnScreen.y;
    return localPos;
}

void PipeGridGraphic::drawPipePiece(const PipeGraphic & pipe)
{
    std::vector<std::pair<sf::Vector2f, sf::Vector2f>> lines;

    enum Side
    {
        TOP,
        RIGHT,
        BOTTOM,
        LEFT,
        CENTER // Not a side, i dont care
    };

    auto getMiddle = [this, pipe](Side side)
    {
        sf::Vector2f center;
        switch (side)
        {
        case TOP: center = sf::Vector2f(pipe.localPos.x + elementSize.x / 2, pipe.localPos.y); break;
        case RIGHT: center = sf::Vector2f(pipe.localPos.x + elementSize.x, pipe.localPos.y + elementSize.y / 2); break;
        case BOTTOM: center = sf::Vector2f(pipe.localPos.x + elementSize.x / 2, pipe.localPos.y + elementSize.y); break;
        case LEFT: center = sf::Vector2f(pipe.localPos.x, pipe.localPos.y + elementSize.y / 2); break;
        case CENTER: center = sf::Vector2f(pipe.localPos.x + elementSize.x / 2, pipe.localPos.y + elementSize.y / 2); break;
        default: assert(false);
        }
        return center;
    };

    if (pipe.type == PipePiece::Straight)
    {
        if (pipe.rotation == PipePiece::None || pipe.rotation == PipePiece::OneEighty)
        {
            lines.emplace_back(getMiddle(LEFT), getMiddle(RIGHT));
        }
        else
        {
            lines.emplace_back(getMiddle(TOP), getMiddle(BOTTOM));
        }
    }
    else if (pipe.type == PipePiece::Angled)
    {
        if (pipe.rotation == PipePiece::None)
        {
            lines.emplace_back(getMiddle(TOP), getMiddle(CENTER));
            lines.emplace_back(getMiddle(CENTER), getMiddle(RIGHT));
        }
        else if (pipe.rotation == PipePiece::Ninety)
        {
            lines.emplace_back(getMiddle(CENTER), getMiddle(RIGHT));
            lines.emplace_back(getMiddle(CENTER), getMiddle(BOTTOM));
        }
        else if (pipe.rotation == PipePiece::OneEighty)
        {
            lines.emplace_back(getMiddle(LEFT), getMiddle(CENTER));
            lines.emplace_back(getMiddle(CENTER), getMiddle(BOTTOM));
        }
        else if (pipe.rotation == PipePiece::TwoSeventy)
        {
            lines.emplace_back(getMiddle(LEFT), getMiddle(CENTER));
            lines.emplace_back(getMiddle(TOP), getMiddle(CENTER));
        }
    }
    else if (pipe.type == PipePiece::Tee)
    {
        if (pipe.rotation == PipePiece::None)
        {
            lines.emplace_back(getMiddle(LEFT), getMiddle(RIGHT));
            lines.emplace_back(getMiddle(CENTER), getMiddle(BOTTOM));
        }
        else if (pipe.rotation == PipePiece::Ninety)
        {
            lines.emplace_back(getMiddle(TOP), getMiddle(BOTTOM));
            lines.emplace_back(getMiddle(LEFT), getMiddle(CENTER));
        }
        else if (pipe.rotation == PipePiece::OneEighty)
        {
            lines.emplace_back(getMiddle(TOP), getMiddle(CENTER));
            lines.emplace_back(getMiddle(LEFT), getMiddle(RIGHT));
        }
        else if (pipe.rotation == PipePiece::TwoSeventy)
        {
            lines.emplace_back(getMiddle(TOP), getMiddle(BOTTOM));
            lines.emplace_back(getMiddle(CENTER), getMiddle(RIGHT));
        }
    }

    for (const auto& line : lines)
    {
        auto [start, end] = line;
        drawRightAngleLine(window, translateToReal(start), translateToReal(end));
    }
}

void PipeGridGraphic::drawGrid()
{    
    for (const auto& pair : pipes)
    {
        const PipeGraphic& pipe = pair.second;
        drawEmptyRect(translateToReal(pipe.localPos), elementSize, sf::Color::White);
    }
}

void PipeGridGraphic::drawEmptyRect(sf::Vector2f pos, sf::Vector2f size, sf::Color color)
{
    sf::VertexArray rectangle(sf::LinesStrip, 5);
    rectangle[0] = pos;
    rectangle[1].position = sf::Vector2f{ pos.x + size.x, pos.y };
    rectangle[2].position = sf::Vector2f{ pos.x + size.x, pos.y + size.y };
    rectangle[3].position = sf::Vector2f{ pos.x, pos.y + size.y };
    rectangle[4].position = pos;

    rectangle[0].color = rectangle[1].color = rectangle[2].color = rectangle[3].color = rectangle[4].color = color;
    window.draw(rectangle);
}