#include "PipeGrid.hpp"
#include <Shared/Random.hpp>
#include <Shared/Exception.hpp>
#include <queue>

PipeGrid::PipeGrid(Size size)
    : gridSize(size)
{

}

void PipeGrid::generate()
{
    clear();
    createEndpoints();
    createSolutionPath();
    populateSolution();
    populateLeftover();
}

void PipeGrid::rotatePipe(Position pipePos)
{
    PipePiece& target = pipes.at(pipePos);
    target.rotateClockwise();
    bool solved = isSolved();

    if (onPipeRotated)
    {
        onPipeRotated(target, solved);
    }
}

void PipeGrid::clear()
{
    solutionStart.invalidate();
    solutionEnd.invalidate();
    solution.clear();
    pipes.clear();
}

void PipeGrid::createEndpoints()
{
    // From left to right for now
    //solutionStart = Position(0, Common::Random::GetInt(0, gridSize.getHeight() - 1));
    //solutionEnd = Position(gridSize.getWidth() - 1, Common::Random::GetInt(0, gridSize.getHeight() - 1));

    solutionStart = { 0, 4 };
    solutionEnd = { 4, 2 };
}

void PipeGrid::createSolutionPath()
{
    enum WalkDirection
    {
        UP,
        RIGHT,
        DOWN,
        LEFT
    };

    std::set<WalkDirection> validDirections;
    solution.push_back(solutionStart);
    Position currentPos = solutionStart;

    auto updateValidDirections = [&validDirections](const Position& from, const Position& to) {
        validDirections.clear();
        if (from.getX() < to.getX())
            validDirections.emplace(RIGHT);
        if (from.getX() > to.getX())
            validDirections.emplace(LEFT);
        if (from.getY() < to.getY())
            validDirections.emplace(DOWN);
        if (from.getY() > to.getY())
            validDirections.emplace(UP);
    };

    auto movePosition = [](const Position& current, WalkDirection direction) {
        Position next = current;
        switch (direction)
        {
        case LEFT: next.setX(next.getX() - 1); break;
        case RIGHT: next.setX(next.getX() + 1); break;
        case UP: next.setY(next.getY() - 1); break;
        case DOWN: next.setY(next.getY() + 1); break;
        default: assert(false);
        }
        return next;
    };

    while (currentPos != solutionEnd)
    {
        updateValidDirections(currentPos, solutionEnd);

        if (validDirections.empty())
        {
            // Should never be empty while walk is not done
            assert(false);
            return;
        }

        WalkDirection direction;
        if (validDirections.size() == 1)
            direction = *validDirections.begin();
        else
        {
            // Random valid direction
            direction = Common::Random::GetElementSet(validDirections);
        }

        currentPos = movePosition(currentPos, direction);
        solution.push_back(currentPos);
    }
}

void PipeGrid::populateSolution()
{
    assert(solution.size() >= 3);

    // Think of a snake where itCurrent is the head, itPrevious is the tail and itNext is where its going to go next
    auto itPrevious = solution.begin();
    auto itCurrent = itPrevious + 1;
    auto itNext = itCurrent + 1;
    
    pipes.emplace(solutionStart, PipePiece(PipePiece::Straight, solutionStart));
    pipes.emplace(solutionEnd, PipePiece(PipePiece::Straight, solutionEnd));

    while (*itCurrent != solutionEnd)
    {
        PipePiece pipe = getValidPipe(*itCurrent, *itNext, *itPrevious);
        pipes.insert({ *itCurrent, pipe });

        itPrevious++;
        itCurrent++;
        itNext++;
    }
}

PipePiece PipeGrid::getValidPipe(Position current, Position next, Position previous) const
{
    // Possible combinations and their respective pipes
    // Using offsets - first is previous pos offset from current pos, second is next pos offset from current pos
    // T pipe can be used anywhere
    static const std::map<std::pair<Position, Position>, PipePiece::Type> combinationsMap =
    {
        // Start    End         Corresponding pipes
        { {{0, -1}, {-1, 0}}, PipePiece::Angled},
        { {{1, 0}, {0, 1}}, PipePiece::Angled},
        { {{-1, 0}, {0, -1}}, PipePiece::Angled},
        { {{1, 0}, {0, -1}}, PipePiece::Angled},
        { {{-1, 0}, {0, 1}}, PipePiece::Angled},
        { {{0, 1}, {1, 0}}, PipePiece::Angled},
        { {{-1, 0}, {1, 0}}, PipePiece::Straight},
        { {{1, 0}, {-1, 0}}, PipePiece::Straight},
        { {{0, -1}, {0, 1}}, PipePiece::Straight},
        { {{0, 1}, {0, -1}}, PipePiece::Straight},
    };

    Position startOff(previous.getX() - current.getX(), previous.getY() - current.getY() );
    Position endOff(next.getX() - current.getX(), next.getY() - current.getY());

    auto it = combinationsMap.find({ startOff, endOff });
    assert(it != combinationsMap.end());
    return PipePiece(it->second, current);
}

void PipeGrid::populateLeftover()
{
    for (int x = 0; x < gridSize.getWidth() - 1; ++x)
    {
        for (int y = 0; y < gridSize.getHeight() - 1; ++y)
        {
            // Only populate empty places, so as to skip solution
            if (pipes.find({ x, y }) == pipes.end())
            {
                pipes[{x, y}] = createRandomizedPipe({ x, y });
            }
        }
    }
}

bool PipeGrid::arePipesConnected(const PipePiece & one, const PipePiece & two) const
{
    std::vector<Position> connections = one.getConnections();
    // Filter out invalid positions
    auto it = std::begin(connections);
    while (it != connections.cend())
    {
        Position p;
        if (p.getX() < 0 || p.getX() >= gridSize.getWidth() ||
            p.getY() < 0 || p.getY() >= gridSize.getHeight())
        {
            it = connections.erase(it);
        }
    }
    return std::find(connections.begin(), connections.end(), two.getPosition()) != connections.end();
}

PipePiece PipeGrid::createRandomizedPipe(const Position & pos)
{
    PipePiece::Type type = static_cast<PipePiece::Type>(Common::Random::GetInt(int(PipePiece::Straight), int(PipePiece::Tee)));
    PipePiece::Rotation rotation = static_cast<PipePiece::Rotation>(Common::Random::GetInt(int(PipePiece::None), int(PipePiece::TwoSeventy)));

    return PipePiece(type, pos, rotation);
}

bool PipeGrid::isSolved() const
{
    auto canMove = [this](const Position& from, const Position& to) -> bool
    {
        // Position hash doesnt work with negative numbers X_X
        if (from.getX() < 0 || from.getY() < 0 || to.getX() < 0 || to.getY() < 0)
            return false;
        else
            return arePipesConnected(pipes.at(from), pipes.at(to));
    };

    Searcher searcher{ canMove, solutionStart, solutionEnd };
    // Will do something with this later
    std::list<Position> blah;
    return searcher.search(blah);
}

Searcher::Searcher(CanMoveToFn canMoveCheckFn, const Position& start, const Position& end)
    : canMoveCallback(canMoveCheckFn)
    , start(start)
    , finish(end)
{
}

bool Searcher::search(std::list<Position>& path)
{
    Node startNode(nullptr, start);

    std::list<Node> nodes = { startNode };
    std::set<Position> checkedPositions;
    std::queue<Node*> toBeSearched;
    toBeSearched.push(&startNode);

    auto filterNeighbours = [this, &checkedPositions](const Position& from, std::set<Position>& neighbours) {
        auto it = std::begin(neighbours);
        while (it != neighbours.end())
        {
            if (!canMoveCallback(from, *it) || checkedPositions.find(*it) != checkedPositions.end())
            {
                it = neighbours.erase(it);
            }
            else
            {
                std::advance(it, 1);
            }
        }
    };
    
    Node* solutionNode = nullptr;
    while (!toBeSearched.empty())
    {
        Node* currentNode = toBeSearched.front();
        toBeSearched.pop();

        if (currentNode->pos == finish)
        {
            solutionNode = currentNode;
            break;
        }
        else
        {
            // Mark this node's position as checked
            checkedPositions.insert(currentNode->pos);
            // Get neighbours and remove checked or invalid neighbours
            std::set<Position> neighbours = currentNode->getNeighbours();
            filterNeighbours(currentNode->pos, neighbours);
            // Create nodes for valid neighbours and queue them for a search
            for (const Position& pos : neighbours)
            {
                auto it = nodes.emplace_back(Node(currentNode, pos));
                toBeSearched.push(&it);
            }
        }
        
    }

    if (solutionNode == nullptr)
    {
        return false;
    }
    else
    {
        std::list<Position> solutionPath;
        Node* currentNode = solutionNode;
        while (currentNode != nullptr)
        {
            solutionPath.push_front(currentNode->pos);
            currentNode = currentNode->parent;
        }
        path = solutionPath;
        return true;
    }
}

std::set<Position> Searcher::Node::getNeighbours()
{
    std::list<Position> offsets = {
        {-1, 0}, // Left
        {0, -1}, // Up
        {1, 0}, // Right
        {0, 1}, // Down
    };

    std::set<Position> neighbors;
    for (const Position& offset : offsets)
    {
        Position neighbor = pos + offset;
        neighbors.insert(neighbor);
    }

    return neighbors;
}