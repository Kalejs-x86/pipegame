#include "PipeGrid.hpp"
#include "PipeGridGraphic.hpp"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

namespace
{
    static const sf::Vector2f windowSize{ 1280, 720 };
    static const sf::Vector2f gridPos{ windowSize.x * 0.05f, windowSize.y * 0.05f}; // 5% margins
    static const sf::Vector2f gridGraphicSize{ windowSize.x * 0.9f, windowSize.y * 0.9f }; // 90% size
    static const Size gridSize{ 12, 12 };
}

#ifdef _DEBUG
int main()
#else
// Winmain here
#endif
{
    sf::RenderWindow sfWindow{ sf::VideoMode(1280, 720), "Pipes" };
    sfWindow.setFramerateLimit(60);

    PipeGrid pipeGrid(gridSize);
    pipeGrid.generate();

    PipeGridGraphic gridGraphic{ sfWindow, gridPos, gridGraphicSize, gridSize };

    gridGraphic.onPipeClicked = [&pipeGrid](const Position pipePos)
    {
        pipeGrid.rotatePipe(pipePos);
    };

    pipeGrid.onPipeRotated = [&gridGraphic, &pipeGrid](const PipePiece rotatedPipe, bool isSolved)
    {
        gridGraphic.updatePipes(pipeGrid.getPipes());
    };

    gridGraphic.updatePipes(pipeGrid.getPipes());

    while (sfWindow.isOpen())
    {
        sf::Event event;
        while (sfWindow.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                sfWindow.close();
            }
            if (event.type == sf::Event::KeyPressed)
            {
                //InfNet->HandleInput(event.key.code);
            }
            if (event.type == sf::Event::MouseButtonReleased)
            {
                if (event.mouseButton.button == sf::Mouse::Left)
                {
                    gridGraphic.handleClick({ static_cast<float>(event.mouseButton.x), static_cast<float>(event.mouseButton.y) });
                }
            }
        }

        sfWindow.clear();
        gridGraphic.draw();

        sfWindow.display();
    }
}