﻿#pragma once
#include "Primitives.hpp"
#include <vector>
#include <map>

struct PipePiece
{
    enum Type
    {
        Straight, // ─
        Angled, // └
        Tee // T
    };

    enum Rotation : int
    {
        None = 0,
        Ninety,
        OneEighty,
        TwoSeventy
    };

    PipePiece() = default;
    PipePiece(Type type, Position pos, Rotation rot = None) : type(type), position(pos), rotation(rot) {};

    inline Rotation getRotation() const noexcept { return rotation; }
    inline void setType(Rotation newRot) noexcept { rotation = newRot; }
    inline Position getPosition() const noexcept { return position; }
    inline void setPosition(Position newPos) noexcept { position = newPos; }
    inline Type getType() const noexcept { return type; }
    inline void setType(Type newType) noexcept { type = newType; }

    inline void rotateClockwise() noexcept
    {
        if (rotation == Rotation::TwoSeventy)
            rotation = None;
        else
        {
            int curRotation = static_cast<int>(rotation);
            curRotation++;
            rotation = static_cast<Rotation>(curRotation);
        }
    }

    inline void rotateCounterClockwise() noexcept
    {
        if (rotation == Rotation::None)
            rotation = TwoSeventy;
        else
        {
            int curRotation = static_cast<int>(rotation);
            curRotation--;
            rotation = static_cast<Rotation>(curRotation);
        }
    }
    
    inline std::vector<Position> getConnections() const
    {
        enum Direction
        {
            LEFT,
            TOP,
            RIGHT,
            BOTTOM
        };
        static const std::map<Direction, Position> offsets = {
            {LEFT, {-1, 0}},
            {TOP, {0, -1}},
            {RIGHT, {1, 0}},
            {BOTTOM, {0, 1}}
        };

        std::vector<Position> connections;
        auto addConnection = [&connections](Direction direction)
        {
            connections.push_back(offsets.at(direction));
        };
        
        if (type == Straight)
        {
            if (rotation == None || rotation == OneEighty)
            {
                addConnection(LEFT);
                addConnection(RIGHT);
            }
            else
            {
                addConnection(TOP);
                addConnection(BOTTOM);
            }
        }
        else if (type == Angled)
        {
            if (rotation == None)
            {
                addConnection(TOP);
                addConnection(RIGHT);
            }
            else if (rotation == Ninety)
            {
                addConnection(RIGHT);
                addConnection(BOTTOM);
            }
            else if (rotation == OneEighty)
            {
                addConnection(LEFT);
                addConnection(BOTTOM);
            }
            else if (rotation == TwoSeventy)
            {
                addConnection(LEFT);
                addConnection(TOP);
            }
        }
        else if (type == Tee)
        {
            if (rotation == None)
            {
                addConnection(LEFT);
                addConnection(RIGHT);
                addConnection(BOTTOM);
            }
            else if (rotation == Ninety)
            {
                addConnection(TOP);
                addConnection(BOTTOM);
                addConnection(LEFT);
            }
            else if (rotation == OneEighty)
            {
                addConnection(LEFT);
                addConnection(TOP);
                addConnection(RIGHT);
            }
            else if (rotation == TwoSeventy)
            {
                addConnection(TOP);
                addConnection(RIGHT);
                addConnection(BOTTOM);
            }
        }
        
        return connections;
    }

private:
    Position position;
    Type type;
    Rotation rotation;
};